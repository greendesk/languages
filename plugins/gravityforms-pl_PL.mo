��          T      �       �      �   #   �      �   )   �      '  @   ?  �  �     Q  M   o     �  #   �     �  7                                           Drop files here or Errors have been highlighted below. Select files There was a problem with your submission. This field is required. This type of file is not allowed. Must be one of the following:  Project-Id-Version: Gravity Forms
POT-Creation-Date: 2016-06-16 15:56+0200
PO-Revision-Date: 2017-09-19 17:23+0200
Last-Translator: 
Language-Team: 
Language: pl_PL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.12
X-Poedit-Basepath: ..
X-Poedit-WPHeader: gravityforms.php
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 Przeciągnij pliki tutaj, lub Pola niewypełnione lub wypełnione błędnie zostały oznaczone na czerwono. Wybierz pliki Proszę uzupełnić wszystkie pola. To pole jest wymagane. Ten typ pliku nie jest dozwolony. Dozwolone formaty to: 